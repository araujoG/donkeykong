import jplay.GameImage;
import jplay.GameObject;
import jplay.Sprite;

public class Martelo extends GameImage {
    private boolean alive;
    private int andar;

    public Martelo(String fileName,double posX, double posY) {
        super(fileName);
        this.x = posX;
        this.y = posY;
        this.alive = true;
    }

    public Martelo(String fileName) {
        super(fileName);
        this.alive = true;
    }

    public void deadMartelo(PJogavel p, long tempo){
        if(this.collided(p.getSprite()) && this.alive) {
            p.setMartelo("m");
            this.alive = false;
            p.setTempoMartelo(tempo);
        }
    }

    @Override
    public void draw() {
        if(this.alive) super.draw();
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }
}
