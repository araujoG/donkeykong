import jplay.Window;

public class Plataforma {
    private final int tam = 15;
    Bloco[] plat = new Bloco[tam];


    public  Plataforma(String direcao, boolean reta, int andar, Window janela){

        if (direcao.compareTo("esq") == 0){
            if (!reta)
                for (int i = 0; i < tam-2; i++) {
                    if(i == 2 || i == 6)
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144 + i * 3, true, true);
                    else if((i == 12 && andar == 1) || i == 11 || i == 7)
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144 + i * 3, true, false);
                    else
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144 + i * 3, false, false);
                }
            else
                for (int i = 0; i < tam - 2; i++) {
                    if(andar != 5 && (i == 3 || i == 9))
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144, true, true);
                    else if(i == 12 && andar == 1)
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144, true, false);
                    else if (i == 6)
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144, true, false);
                    else
                        this.plat[i] = new Bloco(52 * i, janela.getHeight() - andar*144,false, false);
                }
        }
        else if (direcao.compareTo("dir") == 0){
            //14 - i para ele estar na msm posição no vetor do q o q ta em baixo e em cima dele
            if (!reta)
                for (int i = 0; i < tam-2; i++) {
                    if(i == 12 || i == 8)
                        this.plat[14-i] = new Bloco(janela.getWidth()-52-52 * i, janela.getHeight() - andar*144 + i * 3, true, false);
                    else if (i == 3 || i == 7)
                        this.plat[14-i] = new Bloco(janela.getWidth()-52-52 * i, janela.getHeight() - andar*144 + i * 3, true, true);
                    else
                        this.plat[14-i] = new Bloco(janela.getWidth()-52-52 * i, janela.getHeight() - andar*144 + i * 3, false, false);
                }
            else
                for (int i = 0; i < tam - 2; i++) {
                    if((i == 5 || i == 11))
                        //14 - i para ele estar na msm posição no vetor do q o q ta em baixo e em cima dele
                        this.plat[14-i] = new Bloco(janela.getWidth()-52-52 * i, janela.getHeight() - andar*144, true, false);
                    else if (i == 8)
                        this.plat[14-i] = new Bloco(janela.getWidth()-52-52 * i, janela.getHeight() - andar*144, true, true);
                    else
                        this.plat[14-i] = new Bloco(janela.getWidth()-52-52 * i, janela.getHeight() - andar*144, false, false);
                }

        }
        else if ((direcao.compareTo("cima") == 0)){
            for (int i = 0; i < tam-2; i++){
                if (i == 11 || i == 7)
                    this.plat[i] = new Bloco(i*52, janela.getHeight() - andar*144,true,false);
                else
                    this.plat[i] = new Bloco(i*52, janela.getHeight() - andar*144,false,false);
            }
        }
        else if (direcao.compareTo("baixo") == 0){
            for (int i = 0; i < tam; i++){
                if (i == 12)
                    this.plat[i] = new Bloco(i*52, janela.getHeight()-10,true,true);
                else
                    this.plat[i] = new Bloco(i*52, janela.getHeight()-10,false,false);
            }
        }

    }

    public void draw(){
        for (int i = 0; i < tam; i++) {
            if(this.plat[i] != null) this.plat[i].draw();
        }
    }


    public int getTam() {
        return tam;
    }

    public Bloco[] getPlat() {
        return plat;
    }

    public void setPlat(Bloco[] plat) {
        this.plat = plat;
    }
}
