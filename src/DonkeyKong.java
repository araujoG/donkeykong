import jplay.Time;

import java.util.ArrayList;
import java.util.List;

public class DonkeyKong extends Personagem{
    List<Barril> barris = new ArrayList<Barril>();
    private Time time = new Time(0,0, true);
    private double tempoBarril;

    public DonkeyKong(double x, double y){
        super("./sprites/dk.png",8,1000,x ,y);
        this.play();
        tempoBarril = 5;
    }

    public void joga(){
        if (time.getSecond() > tempoBarril){
            time.setSecond(0);
            barris.add(new Barril(this.x+this.width, this.y, false));
        }
        if (time.getSecond() == 3){

        }
    }


    public double getTempoBarril() {
        return tempoBarril;
    }

    public void setTempoBarril(double tempoBarril) {
        this.tempoBarril = tempoBarril;
    }
}
