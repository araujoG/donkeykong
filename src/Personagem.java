
import jplay.GameObject;
import jplay.Sprite;

import java.io.File;


//Clase personagem com 2 construtores distintos

public class Personagem extends Sprite {

    //Arquivo com a sprite, nº de frames, duração da animação
    public Personagem(String fileName, int totalFrames, long TDuration) {
        super(fileName, totalFrames);
        File f = new File(fileName);
        if(!f.exists()) {
            throw new FileException("Não existe tal arquivo");
        }
        this.setTotalDuration(TDuration);
    }

    //igual ao primeiro mas também seta a posição X e Y e para a animação do personagem
    public Personagem(String fileName, int totalFrames, long TDuration, double PosX, double PosY) {
        super(fileName, totalFrames);
        File f = new File(fileName);
        if(!f.exists()) {
            throw new FileException("Não existe tal arquivo");
        }
        this.setTotalDuration(TDuration);
        this.x = PosX;
        this.y = PosY;
        this.stop();
    }

    public Personagem(String fileName, double PosX, double PosY) {
        super(fileName, 1);
        this.setTotalDuration(100);
        this.x = PosX;
        this.y = PosY;
        this.stop();
    }

    public boolean colide(GameObject obj) {
        if (collidedX(obj)&&collidedY(obj)) return true;
        else return false;
    }
    public boolean collidedX(GameObject obj){
        if ((this.x > obj.x && this.x< obj.x+obj.width)||(this.x+this.width > obj.x && this.x+this.width< obj.x+obj.width)) return true;
        else return false;
    }

    public boolean collidedY(GameObject obj){
        if (this.y + this.height >= obj.y && this.y + this.height <= obj.y + obj.height ) return true;
        else return false;
    }

    public int getWidth(){
        return this.width;
    }

}