
import jplay.GameObject;
import jplay.Sprite;
import jplay.Window;

public class Barril extends Personagem {
    private double velocidadeX = 1;
    private boolean colidiu = false;
    private int andar = 5;
    private boolean exist = true;
    private boolean escada;
    private boolean pontuavel = true;


    public Barril(double x, double y, boolean escada) {
        super("./sprites/barrel.png", 8, 900, x, y);
        this.setGravity(0.02D);
        this.play();
        this.setFloor(1000);
        this.escada = escada;
    }

    public int colisãoPlat(Plataforma p){
        for (int i = 0; i < p.plat.length; i++) {
            if (p.plat[i] != null &&  this.colisao(p.plat[i])) {
                return i;
            }
        }
        return -1;
    }


    public void move(Window janela){
        if (this.colidiu == true){
            if (velocidadeX > 0) velocidadeX = 1;
            else velocidadeX = -1;
        }
        if ((this.x+this.width >= janela.getWidth()) || (this.x <= 0)) this.velocidadeX *= -1;
        if (this.colidiu && ((this.andar%2 == 0 && this.x+this.width <= 52*2) || (this.andar%2 != 0 && this.x >= 52*13))){
            if (this.andar != 0 )this.andar--;
            this.colidiu = false;
        }

        if(this.andar == 0 && this.x <= 0) this.exist = false;
        this.x += this.velocidadeX;
    }

    public void moveEscada(Window janela, Bloco bloco){

    }

    public boolean colisao(GameObject objeto){
        if(this.collided(objeto)){
            if (((Bloco) objeto).isTemEscada() == true && Math.random() > 0.91 && ((Bloco) objeto).isSobe() == false && this.x + this.width <= objeto.x + objeto.width && this.x >= objeto.x){
                this.y = this.y - 10;
                this.velocidadeX = -(0.000001*velocidadeX);
                if (this.andar != 0 )this.andar--;
                this.colidiu = false;
                return false;
            }
            this.y = objeto.y-this.height;
            this.setFloor((int) objeto.y);
            this.colidiu = true;
            return true;
        }
        this.setFloor(1000);
        return false;
    }

    public boolean isPontuavel() {
        return pontuavel;
    }

    public void pulado() {
        this.pontuavel = false;
    }


    public double getVelocidadeX() {
        return velocidadeX;
    }

    public void setVelocidadeX(double velocidadeX) {
        this.velocidadeX = velocidadeX;
    }

    public boolean isColidiu() {
        return colidiu;
    }

    public void setColidiu(boolean colidiu) {
        this.colidiu = colidiu;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }

    public boolean isEscada() {
        return escada;
    }

    public void setEscada(boolean escada) {
        this.escada = escada;
    }

    public void setPontuavel(boolean pontuavel) {
        this.pontuavel = pontuavel;
    }
}
