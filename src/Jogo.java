import jplay.*;
import jplay.Window;

import java.awt.*;
import java.util.concurrent.TimeUnit;

import static java.awt.Color.*;


public class Jogo {
    public static void Jogo(String Character, int walkingFrames, int hammerFrames, int ladderFrames) throws InterruptedException {
        GameImage background = new GameImage("./sprites/stage1.png");
        Window janela = new Window(background.width,background.height);
        int fase = 0;
        Placar placar = new Placar(5000,2000,janela.timeElapsed());
        Font fonte = new Font("p",0,45);
        Font font = new Font("MONOSPACED",0,60);
        Fase[] fases = new Fase[2];
        fases[0] = new Fase(janela,1);
        fases[1] = new Fase(janela,2);
        DonkeyKong dk = new DonkeyKong(10   , fases[fase].cenario[5].plat[0].y-75);
        PJogavel personagem = new PJogavel(Character,walkingFrames,hammerFrames,ladderFrames, 600,170, 800);
        Plataforma[] cenario = new Plataforma[5];
        Personagem deisy = new Personagem("./sprites/deisy.png", 20, 2400, 150, fases[fase].cenario[5].plat[0].y-65);
        deisy.play();
        personagem.atualizaChao(background.height-100);
        Keyboard keyboard = janela.getKeyboard();
        Sprite gameOver = new Sprite("./sprites/gameOver.png");
        gameOver.x = 90; gameOver.y = 110;
        Personagem peachCry = new Personagem("./sprites/peach.png", 19, 2400, 38, 255);
        peachCry.play();
        while (true){
            background.draw();
            fases[fase].draw();
            personagem.draw(keyboard);
            dk.draw();
            deisy.draw();
            dk.joga();
            for (Barril barril: dk.barris) {
                personagem.destroiBarril(barril);
                if (!barril.isExist()){
                    dk.barris.remove(barril);
                    break;
                }
                barril.draw();
                barril.fall();
                personagem.testaBarril(barril);
                barril.colisãoPlat(fases[fase].cenario[barril.getAndar()]);
                barril.move(janela);
                barril.update();

            }
            if (keyboard.keyDown(Keyboard.ESCAPE_KEY)) break;

            while(personagem.vida == 0){
                background.draw();
                peachCry.draw();
                gameOver.draw();
                janela.drawText("Pontuação: " + placar + " ", 115, 730, WHITE, font);
                peachCry.update();
                janela.update();
                if (keyboard.keyDown(Keyboard.ESCAPE_KEY)) System.exit(0);
            }

            personagem.move();
            if (fase == 1) {
                personagem.pegaEstrelas(fases[fase].estrelas);
                if (fases[fase].estrelas.isEmpty() && personagem.getSprite().collided(deisy) ){
                    placar.terminouFase();
                    personagem.getSprite().x = 170;
                    personagem.getSprite().y = 800;
                    fase = 0;
                    dk.barris.clear();
                    fases[fase].setMartelos(janela,1);
                    if (dk.getTempoBarril() >= 2) dk.setTempoBarril(dk.getTempoBarril() - 1);
                    background.draw();
                    janela.update();
                    TimeUnit.SECONDS.sleep(1);
                }


            }
            else if (fase == 0 ){
                fases[fase].martelo.deadMartelo(personagem, janela.timeElapsed());
                personagem.timerMartelo(janela);
                if (personagem.getSprite().collided(deisy)) {
                    placar.terminouFase();
                    personagem.getSprite().x = 170;
                    personagem.getSprite().y = 800;
                    fase = 1;
                    dk.barris.clear();
                    fases[fase].setEstrelas(janela);
                    if (dk.getTempoBarril() >= 2) dk.setTempoBarril(dk.getTempoBarril() - 0.5);
                    background.draw();
                    janela.update();
                    TimeUnit.SECONDS.sleep(1);

                }
            }

            placar.pulaBarril(personagem,dk.barris);

            personagem.atualizaDescida();
            personagem.moveY(Keyboard.UP_KEY, Keyboard.DOWN_KEY, personagem.getVelocidade(), keyboard, fases[fase].cenario);
            personagem.atualizaAndar(fases[fase].cenario);
            personagem.colisãoPlat(fases[fase].cenario[personagem.getAndar()]);
            personagem.setBlocoAtual(fases[fase].cenario[personagem.getAndar()].plat[personagem.getBloco()]);
            placar.atualizaPlacar(janela.timeElapsed());
            janela.drawText(placar.toString(),2*janela.getWidth()/5,50, Color.gray,fonte);

            dk.update();
            deisy.update();
            personagem.update(keyboard);
            janela.update();
            janela.delay(5);
        }
        janela.exit();
    }
}
