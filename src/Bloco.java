import jplay.GameImage;

public class Bloco extends GameImage{
    private GameImage escada;
    private boolean temEscada;
    private boolean sobe;

    public Bloco(int x, int y, boolean temEscada, boolean sobe) {
        super("./sprites/bloco.png");
        this.x = x;
        this.y = y;
        this.temEscada = temEscada;
        this.sobe = sobe;
        if(temEscada){
            this.escada = new GameImage("./sprites/stage.png");
            this.escada.x = this.x +2;
            if(sobe) this.escada.y = this.y-this.escada.height;
            else this.escada.y = this.y+this.height;
        }
    }

    @Override
    public void draw() {
        super.draw();
        if (this.temEscada && this.sobe) this.escada.draw();

    }

    public GameImage getEscada() {
        return escada;
    }

    public void setEscada(GameImage escada) {
        this.escada = escada;
    }

    public boolean isTemEscada() {
        return temEscada;
    }

    public void setTemEscada(boolean temEscada) {
        this.temEscada = temEscada;
    }

    public boolean isSobe() {
        return sobe;
    }

    public void setSobe(boolean sobe) {
        this.sobe = sobe;
    }
}
