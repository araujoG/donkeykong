import jplay.Window;

import java.util.List;

public class Placar {
    private int pontos;
    private long intervalo;
    private long tempo;
    private int decrescimo;

    public Placar(int pontos, long intervalo, long tempoInicial){
        this.setIntervalo(intervalo);
        this.setPontos(pontos);
        this.setTempo(tempoInicial);
    }

    public void atualizaPlacar(long tempoAtual){
        if(tempoAtual - tempo > intervalo){
            this.setTempo(tempoAtual);
            this.perdePonto();
        }
    }

    public void pulaBarril(PJogavel personagem, List<Barril> barris){
        for(Barril barril : barris){
            if(personagem.getSprite().isJumping() && barril.getAndar() == personagem.getAndar() && barril.isExist() && barril.isPontuavel()){
                if(barril.x < personagem.getSprite().x + personagem.getSprite().getWidth() && barril.x > personagem.getSprite().x){
                    if(barril.y > personagem.getSprite().y && barril.y < personagem.getSprite().y + 110){
                        this.setPontos(this.getPontos() + 150);
                        barril.pulado();
                    }
                }
            }
        }
    }

    public void terminouFase(){
        this.setPontos(this.getPontos() + 500);
    }

    public void perdePonto() {
        this.setPontos(this.getPontos()-100);
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public long getIntervalo() {
        return intervalo;
    }

    public void setIntervalo(long intervalo) {
        this.intervalo = intervalo;
    }

    public long getTempo() {
        return tempo;
    }

    public void setTempo(long tempo) {
        this.tempo = tempo;
    }

    @Override
    public String toString() {
        return pontos + "";
    }
}
