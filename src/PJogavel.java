
import jplay.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static jplay.Collision.collided;


//Essa classe representa um personagem jogável com 2 sprites(direita e esquerda)
public class PJogavel {

    public final String inicioEnd = "./sprites/";
    public final String extensao = ".png";

    public boolean parado = false;
    public boolean escada = false; //true se estiver em cima de uma escada
    public boolean subindo = false;
    public boolean podeDescer = false;
    public String martelo = "";
    public long tempoMartelo;
    public String direcao = "dir";
    public String tSprite = direcao + martelo;

    public double velocidade = 2D;
    public int vida = 3;
    Sound anda = new Sound("./sounds/walk.wav");
    //Infos de posição
    private int bloco;
    private int andar;
    private Bloco blocoAtual;
    public int floor;

    private Map<String,Personagem> sprites = new HashMap<>();

    //Cria um personagem jogável(construtor)
    public PJogavel(String personagem, int walkingFrames, int hammerFrames, int ladderFrames, long TDuration, double PosX, double PosY){
        String spriteE = inicioEnd + personagem + "Left" + extensao;
        String spriteEM = inicioEnd + personagem + "HammerLeft" + extensao;
        String spriteD = inicioEnd + personagem + "Right" + extensao;
        String spriteDM = inicioEnd + personagem + "HammerRight" + extensao;
        String spriteEscada = inicioEnd + personagem + "Ladder" + extensao;

        this.sprites.put("esq",new Personagem(spriteE,walkingFrames,TDuration,PosX,PosY));
        this.sprites.put("esqm",new Personagem(spriteEM,hammerFrames,TDuration,PosX,PosY));
        this.sprites.put("dir",new Personagem(spriteD,walkingFrames,TDuration,PosX,PosY));
        this.sprites.put("dirm",new Personagem(spriteDM,hammerFrames,TDuration,PosX,PosY));
        this.sprites.put("escada",new Personagem(spriteEscada,ladderFrames,TDuration,PosX,PosY));

        this.floor = (int) this.sprites.get(tSprite).y + this.sprites.get(tSprite).height;
        for(String key : sprites.keySet()) this.sprites.get(tSprite).setFloor(floor);
        for(String key : sprites.keySet()) this.sprites.get(tSprite).setGravity(0.02D);
        for(String key : sprites.keySet()) this.sprites.get(tSprite).setJumpVelocity(2.3D);

    }

    //Método para destruir o barril
    public void destroiBarril(Barril o){ //Substituir GameObject pelo tipo martelo
        if(this.martelo == "m"){
            if(this.sprites.get(tSprite).collided(o)) o.setExist(false);
        }
    }

    //Movimento do personagem em y
    public void moveY(int upKey, int downKey, double velocity, Keyboard keyboard, Plataforma[] cenario) {
        if (blocoAtual != null && blocoAtual.isTemEscada()) {
            this.overEscada(blocoAtual.getEscada());
        }
        if (this.escada) {
            if (keyboard.keyDown(upKey) && blocoAtual.isTemEscada() && this.blocoAtual.isSobe()) {
                this.martelo = "";
                this.sprites.get("dir").y -= 0.5 * velocity;
                this.subindo = true;
            } else if (keyboard.keyDown(downKey) && ((this.blocoAtual.isTemEscada() && !this.blocoAtual.isSobe()) || this.podeDescer)) {
                this.martelo = "";
                this.getSprite().y += 0.5 * velocity;
                this.subindo = true;
            }
        }
        if(this.subindo) this.getSprite().setGravity(0D);
        else this.getSprite().setGravity(0.05D);
    }


    public void overEscada(GameObject o){
        this.escada = this.sprites.get("dir").collidedX(o);
        if(!this.sprites.get("dir").collidedY(o)) this.subindo = false;

    }

    //Movimenta o personagem(só funcionou adotando uma sprite(a direita no caso) como padrão)
    //Colocando tSprite(sprite atual) o personagem ficava sumindo
    public void move() {
        if (!this.subindo) {
            this.getSprite().jump(Keyboard.SPACE_KEY);
            this.sprites.get("dir").moveX(Keyboard.LEFT_KEY, Keyboard.RIGHT_KEY, this.velocidade);
        }
    }

    public void atualizaDescida(){
        if(this.subindo && !this.getSprite().collided(blocoAtual)) this.podeDescer = true;
        else this.podeDescer = false;
    }

    public void atualizaChao(int floor){
        for(Map.Entry<String, Personagem> entry : this.sprites.entrySet()) entry.getValue().setFloor(floor);
    }

    public void atualizaBloco(){
        int tam = 15;
        int xBloco = 52;
        for (int i = 0; i<tam; i++){
            if(this.getSprite().x >= xBloco*i){
                this.setBloco(i);
            }
        }
    }

    public void atualizaAndar(Plataforma[] cenario){
        int tam = cenario.length;
        for (int i = 0; i<tam; i++){
            if(cenario[i].plat[this.bloco] != null) {
                if(this.getSprite().y < cenario[i].plat[this.bloco].y - this.getSprite().height + 5){
                    this.setAndar(i);
                }
            }
        }
    }

    public boolean colisao(GameObject objeto){
        if(this.getSprite().colide(objeto)&& !this.subindo){
            this.getSprite().y = objeto.y-this.getSprite().height;
            this.getSprite().setFloor((int) objeto.y);
            return true;
        }
        this.getSprite().setFloor(1000);
        return false;

    }

    public int colisãoPlat(Plataforma p){
        this.atualizaBloco();
        for (int i = 0; i < p.plat.length; i++) {
            if (p.plat[i] != null &&  this.colisao(p.plat[i])) {
                //this.bloco = i;
                return i;
            }
        }
        return -1;
    }

    public Personagem getSprite(){
        return this.sprites.get("dir");
    }

    public void setY(Double y){
        this.sprites.get("dir").y = y;
    }

    //Atualiza a sprite q ta sendo utilizada e a posição
    public void atualiza(Keyboard keyboard){
        //Atualiza se o personagem ta com o martelo ou não
        //Fazer o collision com o gameobject martelo dps
//        if(keyboard.keyDown(Keyboard.UP_KEY)) this.martelo = "m";
//        if(keyboard.keyDown(Keyboard.DOWN_KEY)) this.martelo = "";

        //Atualiza para que lado o personagem ta virado
        if(!this.subindo){
            if(keyboard.keyDown(Keyboard.LEFT_KEY)) {
                this.direcao = "esq";
                this.parado = false;
            }else if(keyboard.keyDown(Keyboard.RIGHT_KEY)) {
                this.direcao = "dir";
                this.parado = false;
            }else this.parado = true;
        }else {
            this.getSprite().setGravity(0D);
            if(keyboard.keyDown(Keyboard.UP_KEY) || keyboard.keyDown(Keyboard.DOWN_KEY)) {
                this.martelo = "";
                this.parado = false;
            }else this.parado = true;
            this.direcao = "escada";
        }

        this.tSprite = direcao + martelo;

        //Atualiza se o personagem ta parado ou não
        if (this.parado) this.sprites.get(tSprite).stop();
        else {
            //if(anda.isExecuting() == false) anda.play();
            this.sprites.get(tSprite).play();
        }
        //Mantem todas as sprites na msm posição
        for(String key : sprites.keySet()){
            this.sprites.get(key).x = this.sprites.get("dir").x;
            this.sprites.get(key).y = this.sprites.get("dir").y;
        }

    }

    //Igual ao método draw
    public void draw(Keyboard keyboard){
        this.atualiza(keyboard);
        this.sprites.get(tSprite).draw();
        int i, x = 20, y=20;

        for (i=0; i<this.vida; i++){
            Sprite vida = new Sprite("./sprites/life.png");
            vida.x = x;
            vida.y = y;
            vida.draw();
            x = x + 50;
        }
    }

    //Igual ao método update
    public void update(Keyboard keyboard){
        this.sprites.get(tSprite).update();
    }

    //Retorna a vida do personagem
    public int getVida(){
        return this.vida;
    }

    //Diminui 1 de vida do personagem
    public void testaBarril(Barril barril){
        if(this.andar == barril.getAndar() || this.andar+1 == barril.getAndar()){
            if (this.getSprite().collided(barril)){
                this.getSprite().x = 170;
                this.getSprite().y = 800;
                this.atualizaChao(879);
                this.setAndar(0);
                barril.setExist(false);
                this.vida--;

            }
        }

    }

    public void pegaEstrelas(List<GameImage> estrelas){
        for (int i = 0; i < estrelas.size(); i++) {
            if (this.getSprite().collided(estrelas.get(i)))
                estrelas.remove(estrelas.get(i));
        }

    }



    public void timerMartelo(Window janela){
        if(this.martelo == "m"){
            if(janela.timeElapsed() - this.tempoMartelo > 10000){
                this.martelo = "";
            }
        }
    }

    public int getBloco() {
        return bloco;
    }

    public void setBloco(int bloco) {
        this.bloco = bloco;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public Bloco getBlocoAtual() {
        return blocoAtual;
    }

    public void setBlocoAtual(Bloco blocoAtual) {
        this.blocoAtual = blocoAtual;
    }

    public double getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(double velocidade) {
        this.velocidade = velocidade;
    }

    public boolean isSubindo() {
        return subindo;
    }

    public void setSubindo(boolean subindo) {
        this.subindo = subindo;
    }

    public boolean isPodeDescer() {
        return podeDescer;
    }

    public void setPodeDescer(boolean podeDescer) {
        this.podeDescer = podeDescer;
    }

    public String getMartelo() {
        return martelo;
    }

    public void setMartelo(String martelo) {
        this.martelo = martelo;
    }

    public long getTempoMartelo() {
        return tempoMartelo;
    }

    public void setTempoMartelo(long tempoMartelo) {
        this.tempoMartelo = tempoMartelo;
    }
}
