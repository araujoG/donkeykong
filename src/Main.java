import jplay.GameImage;
import jplay.GameObject;
import jplay.Keyboard;
import jplay.Window;

import java.awt.*;
import jplay.*;


/*
@author Gabriel Guerreiro
        Gabriel Araujo
        Jhonathan Azevedo
        cada um fez:
        guerreiro: Menu, game over, organização das sprites, exception e parte do barril
        araujo: thread, Pjogavel, subida e descida de escada, Placar
        jhonatan: Barril, Plataforma e fases
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        try {
            GameImage background = new GameImage("./sprites/stage1.png");
            Window janela = new Window(background.width, background.height);
            Keyboard keyboard = janela.getKeyboard();
            Personagem mario = new Personagem("./sprites/marioChar.png", 4, 800, 81, 260);
            Personagem peach = new Personagem("./sprites/peachChar.png", 5, 1000, 442, 260);
            Sprite selecaoTxt = new Sprite("./sprites/selecao.png", 1);
            selecaoTxt.x = 90;
            selecaoTxt.y = 110;
            Sprite marioTxt = new Sprite("./sprites/marioTxt.png", 1);
            marioTxt.x = 109;
            marioTxt.y = 689;
            Sprite peachTxt = new Sprite("./sprites/peachTxt.png", 1);
            peachTxt.x = 550;
            peachTxt.y = 689;
            mario.play();
            peach.play();
            Mouse mouse = janela.getMouse();
            while (true) {
                if (keyboard.keyDown(Keyboard.ESCAPE_KEY)) break;
                background.draw();
                peach.draw();
                mario.draw();
                selecaoTxt.draw();
                peachTxt.draw();
                marioTxt.draw();
                if (mouse.isOverObject(peach) == true) {
                    peach.update();
                    if (mouse.isLeftButtonPressed() == true) {
                        Jogo.Jogo("peach", 2, 4, 3);
                    }
                }
                if (mouse.isOverObject(mario) == true) {
                    mario.update();
                    if (mouse.isLeftButtonPressed() == true) {
                        Jogo.Jogo("mario", 4, 4, 4);
                    }
                }
                janela.update();
            }
            janela.exit();
        }
        catch(FileException e){
            System.out.println(e.getMessage());
        }
        catch(RuntimeException e){
            System.out.println(e.getMessage());
        }
    }
}
