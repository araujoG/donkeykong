import jplay.GameImage;
import jplay.Keyboard;
import jplay.Window;

import java.util.ArrayList;
import java.util.List;

public class Fase {
    public Plataforma[] cenario;
    public List<GameImage> estrelas;
    //public List<Martelo> martelos;
    public Martelo martelo;
    int fase;

    public Fase(Window janela, int fase){
        cenario = new Plataforma[6];
        this.fase = fase;
        cenario[0] = new Plataforma("baixo", true,0, janela);
        if (fase == 1){
            for (int i = 1; i <= 4; i++) {
                if(i%2 == 0) {
                    cenario[i] = new Plataforma("dir",false, i,janela);
                }
                else {
                    cenario[i] = new Plataforma("esq",false, i,janela);
                }
            }
            cenario[5] = new Plataforma("cima", true,5, janela);

            setMartelos(janela,2);
        }
        if (fase == 2){

            this.estrelas = new ArrayList<GameImage>();
            for (int i = 1; i <= 5; i++) {
                if (i%2 == 0 )
                    cenario[i] = new Plataforma("dir",true,i,janela);
                else
                    cenario[i] = new Plataforma("esq",true,i,janela);
            }
            setEstrelas(janela);

        }



    }

    public void setEstrelas(Window janela){
        this.estrelas = new ArrayList<GameImage>();
        for (int i = 1; i <= 5; i++) {
            this.estrelas.add(new GameImage("./sprites/star.png"));
        }
        int i = 0;
        for (GameImage estrela: estrelas) {
            estrela.x = Math.random()*(janela.getWidth()-estrela.width);
            estrela.y = janela.getHeight() -(i+1)*144 + estrela.height+10;
            i++;
        }
    }

    public void setMartelos(Window janela, int quantidade){
        //this.martelos = new ArrayList<Martelo>();
//        for (int i = 1; i <= quantidade; i++) {
//            this.martelos.add(new Martelo("./sprites/hammer.png"));
//        }
//        int i = 0;
//        for (Martelo m: this.martelos) {
//            m.x = Math.random()*(janela.getWidth()-m.width);
//            m.y = janela.getHeight() -(i+1)*144 + m.height+10;
//            i++;
//        }
        this.martelo = new Martelo("./sprites/hammer.png");
        martelo.x = martelo.width * 4;
        martelo.y = 144 + martelo.height+10;
    }


    public void draw(){
        for (int i = 0; i < 6; i++) {
            cenario[i].draw();
        }
        if(fase == 2){
            for (GameImage estrela: estrelas) {
                estrela.draw();
            }
        }
        if(fase == 1){
//            for(Martelo martelo: this.martelos){
//                martelo.draw();
//            }
            martelo.draw();
        }
    }

}
